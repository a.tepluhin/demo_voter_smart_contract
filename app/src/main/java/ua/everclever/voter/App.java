package ua.everclever.voter;

import android.app.Application;
import android.content.Context;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.http.HttpService;

import java.io.File;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

/**
 * Created by Alexey on 13.05.2018.
 */

public class App extends Application {

    public static File walletFile;

    public static String contractAddress = "0xf03808001ea6b96ae27df23369fde9be18ee195b";
    static String privateKey = "1a8772ee219772b87a885339a56901d820c71812c1fe7803222980565a96ae78";
    public static String hodeAddress = "https://195.201.34.135:8543";
    public static VerySimpleVote contract;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (walletFile != null && walletFile.exists())
            walletFile.delete();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        init();
    }

    public void init() {
        Web3j web3 = Web3jFactory.build(new HttpService(hodeAddress, getBadHttpClient(), false));
        Credentials credentials = Credentials.create(privateKey);
        contract = VerySimpleVote.load(contractAddress, web3, credentials, VerySimpleVote.GAS_PRICE, VerySimpleVote.GAS_LIMIT);
    }

    //Ignore SSL errors
    OkHttpClient getBadHttpClient() {
        try {

            SSLContext sslContext = SSLContext.getInstance("SSL");
            X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    trustManager
            };
            sslContext.init(null, trustAllCerts, new SecureRandom());
            return new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .sslSocketFactory(sslContext.getSocketFactory(), trustManager)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .build();
        } catch (Exception ex) {

        }
        return new OkHttpClient();
    }

}
