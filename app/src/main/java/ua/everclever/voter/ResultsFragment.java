package ua.everclever.voter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.EthFilter;

import java.math.BigInteger;
import java.util.Timer;
import java.util.TimerTask;

import rx.Observable;

public class ResultsFragment extends Fragment {

    Timer timer;


    public ResultsFragment() {
        // Required empty public constructor
    }

    public static ResultsFragment newInstance() {
        ResultsFragment fragment = new ResultsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_results, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EthFilter filter = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, App.contractAddress.substring(2));
        Observable<VerySimpleVote.VotedEventResponse> event = App.contract.votedEventObservable(filter);
        //TODO: Подписка на события не сработала, сделал через таймер
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    final BigInteger ForPoroh = App.contract.ForPoroh().send();
                    final BigInteger ForYulia = App.contract.ForYulia().send();
                    final BigInteger ForNakvasiuk = App.contract.ForNakvasiuk().send();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((TextView) view.findViewById(R.id.txtVotesPoroh)).setText(ForPoroh.toString());
                            ((TextView) view.findViewById(R.id.txtVotesYulia)).setText(ForYulia.toString());
                            ((TextView) view.findViewById(R.id.txtVotesNakvasiuk)).setText(ForNakvasiuk.toString());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },0,5000);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (timer!=null){
            timer.cancel();
            timer = null;
        }
    }

}
