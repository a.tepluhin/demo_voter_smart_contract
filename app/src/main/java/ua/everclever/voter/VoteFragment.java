package ua.everclever.voter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;


public class VoteFragment extends Fragment {
    public VoteFragment() {
        // Required empty public constructor
    }

    public static VoteFragment newInstance() {
        VoteFragment fragment = new VoteFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vote, container, false);
    }

    static android.app.AlertDialog d;

    void vote(final RemoteCall<TransactionReceipt> tran) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            d = new ProgressDialog(getActivity());
                            d.setTitle("Выборы!");
                            d.setMessage("Отправляется транзакция");
                            d.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    d.dismiss();
                                }
                            });
                            d.show();
                        }
                    });
                    final TransactionReceipt response = tran.send();
                    Log.d("Voter", response.getTransactionHash());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            d.dismiss();
                            d = null;
                            new AlertDialog.Builder(getContext())
                                    .setPositiveButton("OK", null)
                                    .setNegativeButton("Etherscan", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Uri uri = Uri.parse("https://rinkeby.etherscan.io/tx/" + response.getTransactionHash());
                                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                            getActivity().startActivity(intent);
                                        }
                                    })
                                    .setTitle("Ваш голос отправлен")
                                    .setMessage(String.format("Если вы хороший человек, то он засчитается\r\n\r\n" +
                                                    "Адрес транзакции: %s\r\n" +
                                                    "Вы можете проследить за выполнением на сайте etherscan.io",
                                            response.getTransactionHash()))
                                    .show();
                        }

                    });
                } catch (final Exception e) {
                    e.printStackTrace();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            d.dismiss();
                            d = null;
                            new AlertDialog.Builder(getContext())
                                    .setTitle("Не удалось отправить голос")
                                    .setMessage(e.getMessage())
                                    .show();
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnVotePoroh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemoteCall<TransactionReceipt> tran = App.contract.votePoroh();
                vote(tran);
            }
        });

        view.findViewById(R.id.btnVoteNakvasiuk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemoteCall<TransactionReceipt> tran = App.contract.voteNakvasiuk();
                vote(tran);
            }
        });

        view.findViewById(R.id.btnVoteYulia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemoteCall<TransactionReceipt> tran = App.contract.voteYulia();
                vote(tran);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
