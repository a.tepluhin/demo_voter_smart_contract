package ua.everclever.voter;

import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.4.0.
 */
public class VerySimpleVote extends Contract {
    private static final String BINARY = "608060405260008054600160ff199091161761ff0019166102001762ff000019166203000017905534801561003357600080fd5b50610274806100436000396000f3006080604052600436106100775763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416630bb40823811461007c578063517c2905146100a3578063a45e1355146100ba578063d728d82e146100cf578063e24c7c82146100e4578063e9c1a3fa146100f9575b600080fd5b34801561008857600080fd5b5061009161010e565b60408051918252519081900360200190f35b3480156100af57600080fd5b506100b8610114565b005b3480156100c657600080fd5b50610091610171565b3480156100db57600080fd5b506100b8610177565b3480156100f057600080fd5b506100916101dd565b34801561010557600080fd5b506100b86101e3565b60025481565b600454601e01421161012557600080fd5b60018054810190819055600054604080519283525160ff909116917f4e3d9edca6a2da356d8d1150a807e92eec97b06cdb23420fe2f4777734d752d5919081900360200190a242600455565b60035481565b600454601e01421161018857600080fd5b6003805460020190819055600054604080519283525160ff6201000090920491909116917f4e3d9edca6a2da356d8d1150a807e92eec97b06cdb23420fe2f4777734d752d5919081900360200190a242600455565b60015481565b600454601e0142116101f457600080fd5b6002805460010190819055600054604080519283525160ff61010090920491909116917f4e3d9edca6a2da356d8d1150a807e92eec97b06cdb23420fe2f4777734d752d5919081900360200190a2426004555600a165627a7a72305820bc8b3c88c38209c9b4f4a4ca7acdc15d2c1d08a4962a758e75335134d795efa30029";

    public static final String FUNC_FORYULIA = "ForYulia";

    public static final String FUNC_VOTEPOROH = "votePoroh";

    public static final String FUNC_FORNAKVASIUK = "ForNakvasiuk";

    public static final String FUNC_VOTENAKVASIUK = "voteNakvasiuk";

    public static final String FUNC_FORPOROH = "ForPoroh";

    public static final String FUNC_VOTEYULIA = "voteYulia";

    public static final Event VOTED_EVENT = new Event("Voted", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint8>() {}),
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
    ;

    protected VerySimpleVote(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected VerySimpleVote(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public RemoteCall<BigInteger> ForYulia() {
        final Function function = new Function(FUNC_FORYULIA, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> votePoroh() {
        final Function function = new Function(
                FUNC_VOTEPOROH, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> ForNakvasiuk() {
        final Function function = new Function(FUNC_FORNAKVASIUK, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> voteNakvasiuk() {
        final Function function = new Function(
                FUNC_VOTENAKVASIUK, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> ForPoroh() {
        final Function function = new Function(FUNC_FORPOROH, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> voteYulia() {
        final Function function = new Function(
                FUNC_VOTEYULIA, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public List<VotedEventResponse> getVotedEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(VOTED_EVENT, transactionReceipt);
        ArrayList<VotedEventResponse> responses = new ArrayList<VotedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            VotedEventResponse typedResponse = new VotedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.candidate = (BigInteger) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.votes = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<VotedEventResponse> votedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, VotedEventResponse>() {
            @Override
            public VotedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(VOTED_EVENT, log);
                VotedEventResponse typedResponse = new VotedEventResponse();
                typedResponse.log = log;
                typedResponse.candidate = (BigInteger) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.votes = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Observable<VotedEventResponse> votedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(VOTED_EVENT));
        return votedEventObservable(filter);
    }

    public static RemoteCall<VerySimpleVote> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(VerySimpleVote.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<VerySimpleVote> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(VerySimpleVote.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static VerySimpleVote load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new VerySimpleVote(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static VerySimpleVote load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new VerySimpleVote(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static class VotedEventResponse {
        public Log log;

        public BigInteger candidate;

        public BigInteger votes;
    }
}
