**Демонстрация методов работы приложения Android с Ethereum Smart-Contract**

| ![](https://gitlab.com/a.tepluhin/demo_voter_smart_contract/raw/master/app/src/main/res/mipmap-hdpi/ic_download.png) | [Voter-1.0-release.apk](https://gitlab.com/a.tepluhin/demo_voter_smart_contract/blob/master/app/release/Voter-1.0-debug.apk) |
|--|--|


 - Код смарт-контракта на языке Solidity: [./smart-contract/VerySimpleVote.sol](https://gitlab.com/a.tepluhin/demo_voter_smart_contract/blob/master/smart-contract/VerySimpleVote.sol)
 - Адрес опубликованного смарт-контракта в сети Rinkeby: 0xf03808001ea6b96ae27df23369fde9be18ee195b
 - Адрес доступной ноды: https://195.201.34.135:8543 (может перестать работать)

Функционал:
 - Вызов методов смарт-контракта:
	 - votePoroh()
	 - voteYulia()
	 - voteNakvasiuk()
 - Чтение и отображение значений полей:
	 - ForPoroh
	 - ForYulia
	 - ForNakvasiuk
 - Дополнительные возможности:
	 - Переход к просмотру транзакции на сайте rinkeby.etherscan.io
