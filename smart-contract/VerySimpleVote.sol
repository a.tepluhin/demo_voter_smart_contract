pragma solidity ^0.4.21;

contract VerySimpleVote{
    
    
    uint8 Poroh = 1;uint8 Yulia = 2; uint8 Nakvasiuk = 3;
    
    uint256 public ForPoroh;
    uint256 public ForYulia;
    uint256 public ForNakvasiuk;
    
    uint256 private lastVote;

    modifier notTooQuick() {
        require(now > lastVote + 30);
        _;
        lastVote = now;
    }
    
    event Voted(uint8 indexed candidate,uint256 votes);

    function votePoroh() public notTooQuick{
        ForPoroh++;
        emit Voted(Poroh,ForPoroh);
    }
    
    function voteYulia() public notTooQuick{
        ForYulia++;
        emit Voted(Yulia,ForYulia);
    }
    
    function voteNakvasiuk() public notTooQuick{
        ForNakvasiuk+=2;
        emit Voted(Nakvasiuk,ForNakvasiuk);
    }
}


